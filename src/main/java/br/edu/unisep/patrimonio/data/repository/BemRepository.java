package br.edu.unisep.patrimonio.data.repository;

import br.edu.unisep.patrimonio.data.entity.Bem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BemRepository extends JpaRepository<Bem, Integer> {
}
